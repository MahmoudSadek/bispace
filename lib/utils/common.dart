import 'dart:ui';

import 'package:flutter/material.dart';

const Color blueColor = Color(0xff17365d);
const Color orangColor = Color(0xffed322e);
const Color greenColor = Color(0xff05acac);
const Color BottomBarColor = Color(0xffedf5f8);
const Color sectionBG1Color = Color(0xFFedf5f8);
const Color sectionBG2Color = Color(0xFFed322e);
const Color sectionBG3Color = Color(0xFF17365d);

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}