
import 'package:bispacetask/model/bage_model.dart';
import 'package:bispacetask/model/medal_model.dart';
import 'package:flutter/material.dart';

class WinnerTile extends StatefulWidget {
  MedalModel modelList;
  WinnerTile(this.modelList);

  @override
  _WinnerTileState createState() =>
      _WinnerTileState(modelList);
}

class _WinnerTileState extends State<WinnerTile> {
  MedalModel modelList;

  _WinnerTileState(this.modelList);


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 2, 10, 0),
      child: Container(
        height: 55,
        decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20))),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ) ,
          child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    width: 60,
                    height: 47,
                    decoration: BoxDecoration(
                        color:Color(0xffEDF5F8) ,
                        borderRadius:BorderRadius.only(
                            topLeft:Radius.circular(10) ,
                            bottomLeft: Radius.circular(10)
                        )),
                    child: Center(child: Text(modelList.rate,style: TextStyle(color: Color(0xffD2A13B),fontWeight: FontWeight.bold,fontSize: 18))),
                  ),
                  SizedBox(width: 10,),
                  Text(modelList.name,style:TextStyle(color: Colors.black,fontWeight: FontWeight.normal,fontSize: 13),),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: Text(modelList.points,style:TextStyle(color: Colors.grey,fontWeight: FontWeight.normal,fontSize: 13),),
              ),
            ],
          ) ,

        ),
      ),
    );
  }

}