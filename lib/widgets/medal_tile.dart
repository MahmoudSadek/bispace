
import 'package:bispacetask/model/bage_model.dart';
import 'package:flutter/material.dart';

class MedalTile extends StatefulWidget {
  String imageUrl; String name;Color color;Color textColors;
  MedalTile(this.imageUrl, this.name,this.color,this.textColors);

  @override
  _MedalTileState createState() =>
      _MedalTileState();
}

class _MedalTileState extends State<MedalTile> {

  _MedalTileState();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Card(
      color: widget.color,
      child: Row(

        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  child:Image.asset(widget.imageUrl),
                  height: 110,
                  width: 125,
                ),
                SizedBox(height: 10,),
                Text(widget.name,style:TextStyle(color: widget.textColors,fontWeight: FontWeight.bold,fontSize: 13),),
                Text("2000 Points",style:TextStyle(color: widget.textColors,fontSize: 10),)

              ],
            ),
          ),
        ],
      ),
    );
  }

}