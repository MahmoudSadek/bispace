
import 'package:bispacetask/model/bage_model.dart';
import 'package:flutter/material.dart';

class BadgeTile extends StatefulWidget {
  BadgeModel modelList;

  BadgeTile(this.modelList);

  @override
  _BadgeTileState createState() =>
      _BadgeTileState(modelList);
}

class _BadgeTileState extends State<BadgeTile> {
  BadgeModel modelList;

  _BadgeTileState(this.modelList);


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Row(
      children: <Widget>[
        SizedBox(width: 10,),
        Container(
          alignment: Alignment.center,
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                child: CircleAvatar(
                  radius: 30.0,
                  child: Image.asset(modelList.image),
                  backgroundColor: Colors.transparent,
                ),
                height: 60,
                width: 60,
              ),
              Text(modelList.name)
            ],
          ),
        ),
      ],
    );
  }

}