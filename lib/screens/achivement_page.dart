import 'package:bispacetask/model/bage_model.dart';
import 'package:bispacetask/model/medal_model.dart';
import 'package:bispacetask/utils/common.dart';
import 'package:bispacetask/widgets/badge_tile.dart';
import 'package:bispacetask/widgets/medal_tile.dart';
import 'package:bispacetask/widgets/winner_tile.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class AchievementsPage extends StatefulWidget {
  @override
  _AchievementsPageState createState() => _AchievementsPageState();
}


class _AchievementsPageState extends State<AchievementsPage> {
  List<BadgeModel> listBadges = [];
  List<MedalModel> lisMedals = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: Container(
            width: 30,
            height: 30,
            child: Image.asset("assets/Back Icon.png", scale: 3,)),

        title: Center(
          child: Container(
              width: 200,
              child: Image.asset("assets/Logo.png")),
        ),

        backgroundColor: Color(0xffEDF5F8),
        actions: <Widget>[
          Container(
              width: 30,
              child: Image.asset("assets/Search Icon.png", scale: 3,)),
          SizedBox(
            width: 20,
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          BackGround(context),
          Body(),
        ],
      ),
    );
  }

  Widget Body() {
    return Container(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10,
              ),

              ProgressView(),
              SizedBox(
                height: 5,
              ),
              BageWidget(),
              Stack(children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 40, 0, 0),
                  child: WinnersView(),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Image.asset("assets/Background-2.png",fit: BoxFit.fill,),)

              ]),
              SizedBox(
                height: 10,
              ),
              BottomScroller()



            ],
          ),
        ));
  }


  Widget BageWidget() {
    return Container(
      height: 100,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, position) {
          return BadgeTile(listBadges[position]);
        },
        itemCount: listBadges.length,
      ),
    );
  }
  Widget WinnersView() {
    return Container(

      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          MedalTile("assets/Silver.png",lisMedals[1].name,sectionBG1Color,Colors.black),
          MedalTile("assets/Gold.png",lisMedals[0].name,Colors.red,Colors.white),
          MedalTile("assets/Bronze.png",lisMedals[2].name,sectionBG1Color,Colors.black),

        ],
      ),
    );


  }
  Widget BottomScroller() {
    return Container(
      height: 300,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        itemBuilder: (context, position) {
          int index = position + 3;
          return WinnerTile(lisMedals[index ]);
        },
        itemCount: lisMedals.length - 3,
      ),
    );
  }
  Widget ProgressView(){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration:  BoxDecoration(
          color: Color(0xff17365D),

          borderRadius:  BorderRadius.all(Radius.circular(3)),),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("PROGRESS OVERALL",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 10),),
              SizedBox(height: 10,),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Row(
                  children: <Widget>[
                    new LinearPercentIndicator(
                      width: 340,
                      lineHeight: 6.0,
                      percent: 0.8,
                      backgroundColor: Colors.white70,
                      progressColor: Colors.orange,
                    ),
                    SizedBox(width: 10),

                    Text("86%",style: TextStyle(color: Colors.white,fontSize: 11,fontWeight: FontWeight.bold),)
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );


  }
  Widget BackGround(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Image.asset("assets/Background.png",fit: BoxFit.fill,),
    );
  }

  void setData() {
    //badges
    listBadges.add(BadgeModel("Badge Name","assets/Badge.png"));
    listBadges.add(BadgeModel("Badge Name","assets/Badge.png"));
    listBadges.add(BadgeModel("Badge Name","assets/Badge.png"));
    listBadges.add(BadgeModel("Badge Name","assets/Badge.png"));
    listBadges.add(BadgeModel("Badge Name","assets/Badge.png"));

    //medals
    lisMedals.add( MedalModel("Omar Khaled","#1","2000 Points"));
    lisMedals.add( MedalModel("Yehyia Ahmed","#2","2000 Points"));
    lisMedals.add( MedalModel("Mazen Zeen","#3","2000 Points"));
    lisMedals.add( MedalModel("Ahmed Hassan","#4","1000 Points"));
    lisMedals.add( MedalModel("Mohamed Zein","#5","950 Points"));
    lisMedals.add( MedalModel("Aya Hassan","#6","900 Points"));
    lisMedals.add( MedalModel("Ahmed Hassan","#7","800 Points"));
  }

}