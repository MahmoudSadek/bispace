import 'package:bispacetask/utils/common.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';

import 'achivement_page.dart';
import 'home_page.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedIndex = 3;
  List<StatefulWidget> widgets = [
    HomePage(),
    HomePage(),
    HomePage(),
    AchievementsPage(),
    HomePage()
  ];


  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: widgets[_selectedIndex],

        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: ImageIcon(AssetImage("assets/unactive00.png"),),
                activeIcon: ImageIcon(AssetImage("assets/active00.png")),
                title: Text('Home')),
            BottomNavigationBarItem(
                icon: ImageIcon(AssetImage("assets/unactive01.png"),),
                activeIcon: ImageIcon(AssetImage("assets/active01.png")),
                title: Text('My Courses')),
            BottomNavigationBarItem(
                icon: ImageIcon(AssetImage("assets/unactive02.png"),),
                activeIcon: ImageIcon(AssetImage("assets/active02.png")),
                title: Text('Wishlist')),
            BottomNavigationBarItem(
                icon: ImageIcon(AssetImage("assets/unactive03.png"),),
                activeIcon: ImageIcon(AssetImage("assets/active03.png")),
                title: Text('Achievement')),
            BottomNavigationBarItem(
                icon: ImageIcon(AssetImage("assets/unactive04.png"),),
                activeIcon: ImageIcon(AssetImage("assets/unactive04.png")),
                title: Text('More', )),

          ],
          currentIndex: _selectedIndex,
          selectedItemColor: orangColor,
          unselectedItemColor: sectionBG3Color,
          backgroundColor: Color(0xffEDF5F8),
          onTap: _onItemTapped,
        ), );
  }
}/*
            BottomNavigationBarItem(
                icon: Image.asset("assets/unactive01.png"),
                activeIcon: Image.asset("assets/active01.png"),
                title: 'My Courses'),
            BottomNavigationBarItem(
                icon: Image.asset("assets/unactive02.png"),
                activeIcon: Image.asset("assets/active02.png"),

                title: 'Wishlist'),
            BottomNavigationBarItem(
                icon: Image.asset("assets/unactive03.png"),
                activeIcon: Image.asset("assets/active03.png"),

                title: 'Achievement'
            ),
            BottomNavigationBarItem(
                icon: Image.asset("assets/unactive04.png"),
                activeIcon: Image.asset("assets/active04.png"),
                title: 'More'),*/